package com.atguigu.demo01.text;

public class AppleFactory implements FruitFactory{
    @Override
    public Apple createFruit() {
        return new Apple();
    }
}
