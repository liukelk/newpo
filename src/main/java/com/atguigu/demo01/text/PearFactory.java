package com.atguigu.demo01.text;

public class PearFactory implements FruitFactory{
    @Override
    public Pear createFruit() {
        return new Pear();
    }
}
