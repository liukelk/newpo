package com.atguigu.demo01.text02;

public class demo03lingxing {
    /**
     * 菱形打印
     */

    public static void main(String[] args) {
        for (int i = 1; i <=6; i++) {
            for (int j = 1; j <=6-i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <=2*i-1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (int i = 5; i >=1; i--) {
            for (int j = 0; j <=5-i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <=2*i-1; j++) {
                System.out.print("*");
            }
            System.out.println();
        }


    }
}
