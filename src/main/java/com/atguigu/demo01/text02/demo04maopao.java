package com.atguigu.demo01.text02;

import java.util.ArrayList;
import java.util.List;

public class demo04maopao {

    /**
     * 冒泡排序
     * @param args
     */
    public static void main(String[] args) {

        int[] b = {0,-1,13,-5,12,3,2,8,-7};

        List ints = show(b);


        System.out.println("ints = " + ints);
    }


    public static List show(int[] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length-1; j++) {
                int c = 0;
                if (a[j]>a[j+1]){
                    c = a[j+1];
                    a[j+1] = a[j];
                    a[j] = c;

                }
            }
        }
        List list = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            list.add(a[i]);

        }
        return list;
    }


}
