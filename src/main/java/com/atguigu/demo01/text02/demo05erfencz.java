package com.atguigu.demo01.text02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class demo05erfencz {

    /**
     * 二分查找法
     * @param args
     */

    public static void main(String[] args) {

        int[] f = {0,2,-3,-6,3,2,4,3,7,5,9,10,13};
        int n = -20;
        Integer show = show(f, n);

        System.out.println("show = " + show);

    }




    public static Integer show(int[] a,int b){
        //排序
        Arrays.sort(a);
        //去重
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            if (!list.contains(a[i])){
                list.add(a[i]);
            }
        }
        //查找
        int size = list.size();

        int w = (size)/2;

        if (list.get(w)==b){
            return list.get(w);
        }

        if (list.get(w)<b){
            for (int i = w+1; i < list.size(); i++) {
                if (list.get(i)==b){
                    return list.get(i);
                }
            }
        }

        if (list.get(w)>b){
            for (int i = w-1; i >= 0; i--) {
                if (list.get(i)==b){
                    return list.get(i);
                }
            }
        }


        return null;
    }




}
