package com.atguigu.demo01.text02;

import java.util.Scanner;

public class demo06busishenti {

    /**
     * 不死神兔
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入查看第几月的神兔");
        int n = sc.nextInt();
        int[] ints = new int[n];

        ints[1]=ints[0]=1;

        for (int i = 2; i<n; i++) {
            ints[i]=ints[i-1]+ints[i-2];
        }
        System.out.println(" 您查看的是第"+n+"月的神兔 总数是"+ints[n-1]);
    }
}
