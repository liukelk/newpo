package com.atguigu.demo01.text02;

public class demo07shusu {
    /**
     * 判断101-200之间有多少个素数，并输出所有素数
     */

    public static void main(String[] args) {

        for (int i = 101; i <=200; i++) {
            if (i % 2!=0 && i % 3!= 0){
                System.out.println(i);
            }
        }
    }
}
